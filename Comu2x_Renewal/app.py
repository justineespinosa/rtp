from chalice import Chalice
import sys
from bson.json_util import dumps, loads
from chalicelib.Chat.controllers.chat_controller import chat_controller

app = Chalice(app_name="dev-comu2x-chat")
app.experimental_feature_flags.update([
    'BLUEPRINTS'
])
app.debug = True

#Register controller routes here
app.register_blueprint(chat_controller)