from chalice import Chalice, Blueprint
from chalicelib.Chat.services.chat_service import Chat_Service
import chalicelib.common.constants as constants
import chalicelib.common.api_result as api_result
import sys
from bson.json_util import dumps, loads

app = Chalice(app_name="dev-comu2x-chat")
app.debug = True

SERVICE = Chat_Service()

chat_controller = Blueprint(__name__)

@chat_controller.route('/get_all_chat_thread/{owner_id}/{current_pagination}', methods=["GET"], cors=True)
def get_chat_thread(owner_id, current_pagination):
    response = None
    chat_thread_model = None

    try:
        chat_thread_model = SERVICE.get_all_chat_thread(owner_id, current_pagination)
        response =  api_result.success(dumps(chat_thread_model), "Successfully fetched page {current_pagination} of chat thread for user {owner_id}")
    except Exception as e:
        response =  api_result.error(str(e), "Error in fetching page {current_pagination} of chat thread for user {owner_id}")
    return response


@chat_controller.route('/delete_chat_thread', methods=['POST'], content_types=['application/json'], cors=True)
def delete_chat_thread():
    request = chat_controller.current_request.json_body
    thread_id = request["threadId"]
    user_id = request["userId"]
    is_deleted = False
    try:
        is_deleted = SERVICE.delete_chat_thread(user_id, thread_id)
        response =  api_result.success(is_deleted, "Chat Thread {thread_id} was successfully deleted.")
    except:
        response =  api_result.error(is_deleted, "Chat Thread {thread_id} was not deleted.")

    return response


@chat_controller.route('/get_all_friends/{user_id}/{current_pagination}', methods=["GET"], cors=True)
def get_all_friends(user_id, current_pagination):
    response = None
    friends_model = None

    try:
        friends_model = SERVICE.get_all_friends(user_id, current_pagination)
        response =  api_result.success(dumps(friends_model), "Successfully fetched page {current_pagination} of friends for user {user_id}.")
    except Exception as e:
        response =  api_result.error(str(e), "Error in fetching page {current_pagination} of friends for user {user_id}")

    return response


@chat_controller.route('/get_specific_chat_thread', methods=['POST'], content_types=['application/json'], cors=True)
def get_specific_chat_thread():
    request = chat_controller.current_request.json_body
    owner_id = request["ownerId"]
    current_pagination = request["currentPagination"]
    recipient_id = request["recipientId"]
    thread_id = request["threadId"]
    response = None
    chat_model = None

    try:
        chat_model = SERVICE.get_specific_chat_thread(owner_id, recipient_id, thread_id, current_pagination)
        response =  api_result.success(chat_model, "Successfully fetched page {current_pagination} of chat thread for user {owner_id}.")
    except Exception as e:
        response =  api_result.error(str(e), "Error in fetching page {current_pagination} of chat thread for user {owner_id}.")

    return response


@chat_controller.route('/search_friend', methods=['POST'], content_types=['application/json'], cors=True)
def search_friend():
    request = chat_controller.current_request.json_body
    handle_name = request['handleName']
    current_pagination = request['currentPagination']
    user_id = request['userId']
    response = None
    friends_model = None

    try:
        friends_model = SERVICE.search_friend(handle_name, user_id, current_pagination)
        response =  api_result.success(dumps(friends_model), "Successfully fetched page {current_pagination} of user friends for user {user_id}.")
    except Exception as e:
        response =  api_result.error(str(e), "Error in fetching page {current_pagination} of user friends for user {user_id}.")

    return response
