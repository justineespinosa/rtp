from pymongo import MongoClient
import chalicelib.common.constants as constants
import chalicelib.config.settings as settings
from chalicelib.common.common_service import CommonService
from chalice import Chalice, BadRequestError
from bson.json_util import dumps, loads
from gremlin_python.structure.graph import Graph
from gremlin_python.process.graph_traversal import __
from gremlin_python.process.traversal import T
from gremlin_python.driver.driver_remote_connection import DriverRemoteConnection
from gremlin_python.process.traversal import Cardinality
from bson import ObjectId
import sys
from chalicelib.utils.mongo_connect import db
from chalicelib.utils.neptune_connect import setup_neptune
from botocore.exceptions import ClientError
import logging
import boto3

COMMON_SERVICE = CommonService()


class Chat_Service():
    def get_all_chat_thread(self, owner_id, current_pagination):
        result = None
        page_size = constants.CHAT_THREAD_PAGINATION_SIZE
        skips = COMMON_SERVICE.skip_limit(page_size, current_pagination)
        neptune_db = setup_neptune()
        blocked_ids = neptune_db.V().has("userId", int(owner_id)).both("is_blocked").values("userId").toList()

        try:
            result = db.ChatThread.aggregate([
                {"$match": {"ownerId": int(owner_id)}},
                {"$match": {"recipientId": {"$nin" : blocked_ids}}},
                {"$sort" : {"updateDt" : constants.SORT_DESC}},
                {"$skip" : skips},
                {"$limit" : page_size},
                {
                    "$lookup": {
                        "from": "ChatItems",
                        "localField": "_id",    
                        "foreignField": "threadId", 
                        "as": "chat"
                    }
                },
                {
                    "$project": {
                        "_id": 1,
                        "ownerId": 1,
                        "recipientId": 1,
                        "recipientHandleName": 1,
                        "seen": 1,
                        "createDt": 1,
                        "updateDt": 1,
                        "chat": {"$slice": ["$chat", -1]}
                    }
                },
             ])
        except Exception as e:
            raise e
        return result

    def delete_chat_thread(self, user_id, thread_id):
        result = None
        try:
            #Delete from Chat Thread
            db.ChatThread.delete_one({'_id': ObjectId(thread_id)})

            #Delete from Chat Items
            db.ChatItems.delete_many({"threadId" : ObjectId(thread_id)})

            #Delete from Attachment
            db.Attachments.update({"userId" : int(user_id)}, {"$pull" : { "chat" : {"threadId" : ObjectId(thread_id)}}})

            #Delete from s3 bucket
            chat_prefix = settings.CHAT_BUCKET_FOLDER + str(user_id) + "/" +  str(thread_id)
            s3 = boto3.resource('s3')
            bucket = s3.Bucket(settings.BUCKET_NAME)
            bucket.objects.filter(Prefix=chat_prefix).delete()

            #Delete from thumbnails
            thumbnail_prefix = settings.CHAT_THUMBNAIL_FOLDER + str(user_id) + "/" +  str(thread_id)
            bucket.objects.filter(Prefix=thumbnail_prefix).delete()

            result = True
        except ClientError as e:
            logging.error(e)
            result = False
        except:
            result = False
        return result

    def get_all_friends(self, user_id, current_pagination):
        result = None
        page_size = constants.USERS_PAGINATION_SIZE
        skips = COMMON_SERVICE.skip_limit(page_size, current_pagination)
        neptune_db = setup_neptune()

        try:
            friends = neptune_db.V().has("userId", int(user_id)).both("is_friend").values("userId").toList()
            
            result = db.User.find({"userId": {"$in": friends}},
                 {"userId": 1, "handleName": 1 }).skip(skips).limit(page_size)
        except Exception as e:
            raise e

        return result

    def get_specific_chat_thread(self, owner_id, recipient_id, thread_id, current_pagination):
        result = None
        _id = None
        page_size = constants.CHAT_PAGINATION_SIZE
        skips = COMMON_SERVICE.skip_limit(page_size, current_pagination)

        try:
            if not (thread_id is None) or thread_id:
                result = db.ChatItems.aggregate([
                    {"$match" : {"threadId" : ObjectId(thread_id)}},
                    {"$sort" : {"createDt": constants.SORT_DESC}},
                    {"$skip" : skips},
                    {"$limit" : page_size}
                ])

            if thread_id is None or not thread_id:
                thread = db.ChatThread.find({"ownerId" : int(owner_id), "recipientId" : int(recipient_id)}, {"_id" : 1})

                for record in thread:
                    _id = record["_id"]

                result = db.ChatItems.aggregate([
                    {"$match" : {"threadId" : _id}},
                    {"$sort" : {"createDt" : constants.SORT_DESC}},
                    {"$skip" : skips},
                    {"$limit" : page_size}
                ])
        except Exception as e:
            raise e
        
        if result is None:
            return result
        else:
            return dumps(result)

    def search_friend(self, handle_name, user_id, current_pagination):
        result = None
        page_size = constants.USERS_PAGINATION_SIZE
        skips = COMMON_SERVICE.skip_limit(page_size, current_pagination)
        neptune_db = setup_neptune()

        try:
           friends = neptune_db.V().has("userId", int(user_id)).both("is_friend").values("userId").toList()
           result = db.User.aggregate([
                {"$match": {"handleName":  {"$regex" : handle_name , "$options" :'i'}}},
                {"$match": {"userId": {"$in" : friends}}},
                {"$project": {
                    "userId": 1,
                    "handleName": 1,
                   }},
                {"$skip": skips},
                {"$limit": page_size}
            ])
        except Exception as e:
            raise e

        return result
