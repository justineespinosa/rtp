import chalicelib.common.constants as constants

def success(data: object, message :str):
    return{
        "status": constants.STATUS_SUCCESS,
        "data": data,
        "message": message
    }

def error(data, message):
    return{
        "status": constants.STATUS_ERROR,
        "data": data,
        "message": message
    }