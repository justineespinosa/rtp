import json


class CommonService():

    def skip_limit(self, page_size, page_num):
        skips = (int(page_size) * (int(page_num)-1))
        return skips
