#App Settings
APP_NAME = 'dev-comu2x-chat'

#Mongo Settings
MONGO_CLIENT_URI = 'mongodb://comu2x:Alliance123@comu2xdb-2020-04-14-01-34-06.cluster-cv5fas6s0jdx.ap-northeast-1.docdb.amazonaws.com:27017/?ssl=true&ssl_ca_certs=chalicelib/rds-combined-ca-bundle.pem&replicaSet=rs0&readPreference=secondaryPreferred&retryWrites=false'
MONGO_CLIENT_NAME = 'Comu2x-Document'

#Neptune Settings
NEPTUNE_CONN_STRING = 'wss://comux2dev.cv5fas6s0jdx.ap-northeast-1.neptune.amazonaws.com:8182/gremlin'

#S3 Bucket Settings
BUCKET_NAME = "comu2x-user"
CHAT_BUCKET_FOLDER = "Chat/"
CHAT_THUMBNAIL_FOLDER = "Thumbnail-Chat/"