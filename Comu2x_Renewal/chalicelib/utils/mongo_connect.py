from pymongo import MongoClient
import chalicelib.config.settings as config

client = MongoClient(config.MONGO_CLIENT_URI)
db = client[config.MONGO_CLIENT_NAME]