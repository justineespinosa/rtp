from chalice import BadRequestError
from gremlin_python import statics
from gremlin_python.structure.graph import Graph
from gremlin_python.process.graph_traversal import __
from gremlin_python.process.strategies import *
from gremlin_python.process.traversal import T
from gremlin_python.driver.driver_remote_connection import DriverRemoteConnection
from gremlin_python.process.traversal import Cardinality

import chalicelib.config.settings as config

def setup_neptune():
        neptune_conn_string = config.NEPTUNE_CONN_STRING
        try:
            graph = Graph()
            remote_conn = DriverRemoteConnection(neptune_conn_string, 'g')
            g = graph.traversal().withRemote(remote_conn)
        except Exception as e:
            raise BadRequestError("Could not connect to Neptune. Error: " + str(e))
        return g
